package com.example.compose.controller;

import com.example.compose.entity.CounterEntity;
import com.example.compose.service.CounterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HelloContorller {

    private final CounterService service;
    @GetMapping("/hello")
    public String getMessage(){
        String message = null;
        CounterEntity entity = null;
        try{
            entity = service.findById(1L);
            message = "Hello, the counter is "+entity.getCount();

        }
        catch(Exception e){
            log.trace(e.getMessage());
        }

    return message;
    }
}
