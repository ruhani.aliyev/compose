package com.example.compose.service;


import com.example.compose.entity.CounterEntity;

public interface CounterService {
    CounterEntity save(CounterEntity entity);
    CounterEntity findById(long id);


}
