package com.example.compose.service;

import com.example.compose.entity.CounterEntity;
import com.example.compose.repository.CounterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Service
public class CounterServiceImpl  implements CounterService{

    private final CounterRepository repository;

    @Override
    public CounterEntity save(CounterEntity entity) {
        return repository.save(entity);
    }

    public CounterEntity findById(long id){
       CounterEntity entity = repository.findById(id).orElse(new CounterEntity(0));
        entity.setCount(entity.getCount()+1);
        return repository.save(entity);

    }
}
