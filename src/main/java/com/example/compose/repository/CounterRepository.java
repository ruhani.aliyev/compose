package com.example.compose.repository;

import com.example.compose.entity.CounterEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CounterRepository extends JpaRepository<CounterEntity, Long> {
    CounterEntity save(CounterEntity entity);



}
